import React from 'react';
import Intro from './Components/Intro';
import './App.css';
import Menu from './Components/Menu';
import About from './Components/About';
import Contact from './Components/Contact';
import Products from './Context/Products';

const App = () => {

  
  return (
    <Products>
      <Intro/>
      <Menu/>
      <About/>
      <Contact/>
    </Products>
  )
}

export default App