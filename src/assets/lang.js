const lang = {
    en: {
        menubar: [
            {
                name: "Intro",
                slug: "intro"
            },
            {
                name: "Menu",
                slug: "menu"
            },
            {
                name: "About",
                slug: "about"
            },
            {
                name: "Contact",
                slug: "contact"
            },
        ]
    },
    sq: {
        menubar: [
            {
                name: "Hyrja",
                slug: "intro"
            },
            {
                name: "Menu",
                slug: "menu"
            },
            {
                name: "Per ne",
                slug: "about"
            },
            {
                name: "Kontakto",
                slug: "contact"
            },
        ]
    }
}

export default lang;