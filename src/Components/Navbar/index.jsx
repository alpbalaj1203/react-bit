import React, { useContext, useState } from 'react'
import { useEffect } from 'react'
import en from '../../assets/img/ThankYouUSA.svg'
import sq from '../../assets/img/ShqipeBabo.svg'
import { Context } from '../../Context/Products'
import lang from '../../assets/lang'

const Navbar = () => {

  const [y, sety] = useState();

  const [{language}, dispatch] = useContext(Context);

  const handleScroll = () => {
    if(window.scrollY > 100){
      sety(true);
    }else{
      sety(false);
    }
  }

  const changeLang = () => {
    const langKey = language === 'en' ? "sq" : "en";
    localStorage.setItem("lang", langKey);
    console.log("xsacaec",langKey );
    dispatch({
      type:"LANG",
      payland:{
        language: langKey
      }
    });

  }

  useEffect(() => {
    sety(window.scrollY);
    window.addEventListener("scroll", (e) => handleScroll(e));
  },[]);

  return (
    <nav id="tm-nav" className={`fixed w-full ${y? "scroll" : "" }`}>
            <div className="tm-container mx-auto px-2 md:py-6 text-right">
                <button className="md:hidden py-2 px-2" id="menu-toggle"><i className="fas fa-2x fa-bars tm-text-gold"></i></button>
                <ul className="mb-3 md:mb-0 text-2xl font-normal flex justify-end flex-col md:flex-row">
                  {
                    lang[language].menubar.map((el) => (
                      <li className="inline-block mb-4 mx-4"><a href={`#${el.slug}`} className="tm-text-gold py-1 md:py-3 px-4">{el.name}</a></li>
                    ))
                  }
                    {/* <li className="inline-block mb-4 mx-4"><a href="#intro" className="tm-text-gold py-1 md:py-3 px-4">Intro</a></li>
                    <li className="inline-block mb-4 mx-4"><a href="#menu" className="tm-text-gold py-1 md:py-3 px-4">Menu</a></li>
                    <li className="inline-block mb-4 mx-4"><a href="#about" className="tm-text-gold py-1 md:py-3 px-4">About</a></li>
                    <li className="inline-block mb-4 mx-4"><a href="#contact" className="tm-text-gold py-1 md:py-3 px-4">Contact</a></li> */}
                    <li className="inline-block mb-4 mx-4" onClick={() => changeLang()}>
                      {language === 'sq'? 
                      <img className='flag' src={en} alt="" /> :
                      <img className='flag' src={sq} alt="" /> 
                      }
                        
                    </li>
                </ul>
            </div>            
        </nav>
  )
}

export default Navbar