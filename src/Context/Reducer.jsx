const Reducer = (state, action) => {
    switch(action.type) {
        case "LANG": 
            return {
                ...state,
                language: action.payland.language,
            }
        
        default:
            return state;
    }
};

export default Reducer;